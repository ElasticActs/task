$(document).ready(function() {
    // --Bof JQuery to verify the OS type-->

    // Is iOs?
    var isIOS = /iPad|iPhone|iPod/i.test(navigator.userAgent);
    // Is Android?
	var isAndroid = /Android/i.test(navigator.userAgent);
	if (!isIOS && !isAndroid) {
		$("body").attr('data-window', 'NO mobile');
	} else if (isIOS) {
        $("body").attr('data-window', ' iOS');
        $("div#slider-area").each(function() {
            $(this).find(".slides").addClass('div-mobile');
        });
	} else if (isAndroid) {
        $("body").attr('data-window', 'Android');
        $("div#slider-area").each(function() {
            $(this).find(".slides").addClass('div-mobile');
        });
    }

    // --Bof JQuery for Preventing click activate-->
	$('a[href^="#"]').click(function(et) {
		et.preventDefault();

		return false;
	});

    // --Bof JQuery to verify the width of window-->
    // Retrieve current document width
	var documentWidth = $(window).width();
	$("body").attr('data-started-width', documentWidth);

	$(window).on('resize', function() {
        // Retrieve current window width
		var windowWidth = $(window).width();
		$("body").attr('data-window-width', windowWidth);
    }).trigger('resize');

    $('form#loginForm').submit(function(event) {
        event.preventDefault(); 
        let email = $("input#inputEmail").val();
        let password = $("input#inputPassword").val();
        
        $this = $("#submitButton");
        $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages

        $.ajax({
          url: "/login",
          type: "POST",
          data: {
            email: email,
            password: password
          },
          cache: false,
          success: function(data) {
            if (data) {
                let newData = JSON.parse(data);
                // remove all of tokens
                localStorage.removeItem('authToken');
                localStorage.removeItem('authId');
                // assign new token
                localStorage.setItem('authToken', newData['userToken']);
                localStorage.setItem('authId', newData['userId']);

                //clear all fields
                $('#loginForm').trigger("reset");
    
                // Success than redirect
                $(location).attr('href', '/userinfo/' + newData['userId']);
            } else {
                // Fail message
                $('#success').html("<div class='alert alert-danger loginInfo'>");
                $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
                $('#success > .alert-danger').append($("<strong>").text("Sorry, email or password is not correct!"));
                $('#success > .alert-danger').append('</div>');
                //clear all fields
                $('#loginForm').trigger("reset");
            }
          },
          complete: function() {
            setTimeout(function() {
                $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete
            }, 1000);

            setTimeout(function() {
                $( ".loginInfo" ).remove();
            }, 5000);
          }
        });
    });

    $('form#updateForm').submit(function(event) {
        event.preventDefault(); 

        let id = $("input#inputId").val();
        let firstName = $("input#inputFirst").val();
        let lastName = $("input#inputLast").val();
        let email = $("input#inputNewEmail").val();
        let dob = $("input#inputDoB").val();
        let password = $("input#inputNewPassword").val();
        let token = localStorage.getItem('authToken');

        $this = $("#updateButton");
        $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages

        $.ajax({
            url: "/user",
            type: "POST",
            headers: {
                'Authorization':'Bearer ' + token
            },
            data: {
                id: id,
                firstName: firstName,
                lastName: lastName,
                email: email,
                dob: dob,
                password: password
            },
            cache: false,
            success: function(data) {
                if(data['code'] == 200) {
                    // Success message
                    $('#updateSuccess').html("<div class='alert alert-success updateInfo'>");
                    $('#updateSuccess > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#updateSuccess > .alert-success')
                        .append("<strong>Your information has been updated. </strong>");
                    $('#updateSuccess > .alert-success')
                        .append('</div>');
                } else {
                    // Fail message
                    $('#updateSuccess').html("<div class='alert alert-danger updateInfo'>");
                    $('#updateSuccess > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#updateSuccess > .alert-danger').append($("<strong>").text("Sorry, something went to wrong! "));
                    $('#updateSuccess > .alert-danger').append('</div>');
                }
            },
            complete: function() {
                setTimeout(function() {
                    $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete
                    $( ".updateInfo" ).remove();
                }, 2000);
            }
        });
    });      

    $('form#registerForm').submit(function(event) {
        event.preventDefault(); 

        let firstName = $("input#inputFirst").val();
        let lastName = $("input#inputLast").val();
        let email = $("input#inputNewEmail").val();
        let dob = $("input#inputDoB").val();
        let password = $("input#inputNewPassword").val();

        $this = $("#registerButton");
        $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages

        $.ajax({
            url: "/user",
            type: "POST",
            data: {
                firstName: firstName,
                lastName: lastName,
                email: email,
                dob: dob,
                password: password
            },
            cache: false,
            success: function(data) {
                if (data['code'] == 200) {
                    console.log(data);
                    // Success message
                    $('#registerSuccess').html("<div class='alert alert-success'>");
                    $('#registerSuccess > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#registerSuccess > .alert-success')
                        .append("<strong>Your information has been registered. </strong><br>We sent an email to confirm your email.<br>You can log in after confirmation.");
                    $('#registerSuccess > .alert-success')
                        .append('</div>');
                } else {
                    // Fail message
                    $('#registerSuccess').html("<div class='alert alert-danger registerInfo'>");
                    $('#registerSuccess > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#registerSuccess > .alert-danger').append($("<strong>").text("Sorry, something went to wrong! "));
                    $('#registerSuccess > .alert-danger').append('</div>');
                }
            },
            complete: function() {
                setTimeout(function() {
                    $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete
                    $( ".registerInfo" ).remove();
                    //clear all fields
                    $('#registerForm').trigger("reset");
                }, 2000);
            }
        });
    });      

    $("#inputNewEmail").keyup(function() {
        let btnName = 'btnName';
        let email = $("input#inputNewEmail").val();

        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if ($("#updateButton").length == 1) {
            btnName = 'updateButton';
        }
        
        if ($("#registerButton").length == 1) {
            btnName = 'registerButton';
        }

        if (btnName !== 'btnName') {
            let button = $("#"+btnName);
            if (regex.test(email.toLowerCase())) {
                $( ".msg" ).remove();
                button.prop("disabled", false);
    
                $.ajax({
                    url: "/check-email",
                    type: "POST",
                    data: {
                        email: email.toLowerCase()
                    },
                    cache: false,
                    success: function(data) {
                        // Success message
                        if (data['code'] == 204) {
                            button.prop("disabled", true);
                            $('#validationMessage').html("<div class='msgEmail'>");
                            $('#validationMessage > .msgEmail')
                                .append(data['message']);
                            $('#validationMessage > .msgEmail')
                                .append('</div>');
                        }

                        if (data['code'] == 200) {
                            button.prop("disabled", false);
                            $( ".msgEmail" ).remove();
                        }
                    }
                });
            } else {
                button.prop("disabled", true);
    
                $('#validationMessage').html("<div class='msg'>");
                $('#validationMessage > .msg')
                    .append("Please use proper email address");
                $('#validationMessage > .msg')
                    .append('</div>');
            }            
        }
    });

    $("#inputNewPasswordAgain").keyup(function() {
        let idName = 'idName';
        let password = $("input#inputNewPassword").val();
        let passwordAgain = $("input#inputNewPasswordAgain").val();

        if (password !== passwordAgain) {
            if ($("#updateSuccess").length == 1) {
                idName = 'updateSuccess';
            }

            if ($("#registerSuccess").length == 1) {
                idName = 'registerSuccess';
            }

            if (idName !== 'idName') {
                $('#'+ idName).html("<div class='alert alert-danger passInfo'>");
                $('#'+ idName +' > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                    .append("</button>");
                $('#'+ idName +' > .alert-danger').append($("<strong>").text("Password does not match! "));
                $('#'+ idName +' > .alert-danger').append('</div>');   
            }
        } else {
            $(".passInfo").remove();
        }
    });

    $("input#inputNewPassword").keyup(function () {  
        $('#strengthMessage').html(checkStrength($('input#inputNewPassword').val()));
    });
    function checkStrength(password) {  
        var strength = 0  
        if (password.length < 4) {  
            $('#strengthMessage').removeClass(); 
            $('#strengthMessage').addClass('Short');
            return 'Too short';
        }  

        if (password.length > 3) {
            strength += 1;
        } 
        // If password contains both lower and uppercase characters, increase strength value.  
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
            strength += 1;
        } 
        // If it has numbers and characters, increase strength value.  
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
            strength += 1;
        }  
        // If it has one special character, increase strength value.  
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
            strength += 1;
        }  
        // If it has two special characters, increase strength value.  
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) {
            strength += 1;
        }  
        // Calculated strength value, we can return messages  
        // If value is less than 2  
        if (strength < 2) {  
            $('#strengthMessage').removeClass();
            $('#strengthMessage').addClass('Weak');
            return 'Weak';
        } else if (strength == 2) {  
            $('#strengthMessage').removeClass();  
            $('#strengthMessage').addClass('Good');
            return 'Good';
        } else {   
            $('#strengthMessage').removeClass();
            $('#strengthMessage').addClass('Strong');
            return 'Strong';
        }  
    }

    $('#logOut').click(function(event) {
        event.preventDefault(); 

        $.ajax({
            url: "/logout",
            type: "GET",
            cache: false,
            success: function(data) {
                // Success message
                if (data['code'] == 200) {
                    // remove all of tokens
                    localStorage.removeItem('authToken');
                    localStorage.removeItem('authId');
                    
                    $( location ).attr("href", '/');
                }
            }
        });
    });
});


// --Bof for HTML5 placeholder fix-->
$('[placeholder]').focus(function() {
    var input = $(this);
    if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
    }
}).blur(function() {
    var input = $(this);
    if (input.val() == '' || input.val() == input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
    }
}).blur().parents('form').submit(function() {
    $(this).find('[placeholder]').each(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
                input.val('');
        }
    })
});
// --Eof for HTML5 placeholder fix-->
