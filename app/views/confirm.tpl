[@header/]
<main class="main-container container">

    <!-- Main Row -->
    <div class="row no-gutters">

      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            [@mTitle/]
            [@message/]
          </div>

        </div>
      </div>

    </div>
</main>
[@footer/]
