[@header/]
<main class="main-container container">

    <!-- Main Row -->
    <div class="row no-gutters">

      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-left">User Information</h5>
            <form id="updateForm" class="form-signin">
              <div class="form-label-group">
                <input type="text" id="inputFirst" class="form-control" value="[@members_firstName/]">
                <label for="inputFirst">First Name</label>
              </div>

              <div class="form-label-group">                
                <input type="text" id="inputLast" class="form-control" value="[@members_lastName/]">
                <label for="inputLast">Last Name</label>
              </div>

              <div class="form-label-group">                
                <input type="text" id="inputNewEmail" class="form-control" value="[@members_email/]">
                <label for="inputNewEmail">Email</label>
                <div id="validationMessage"></div>
              </div>

               <div class="form-label-group">                
                <input type="date" id="inputDoB" class="form-control" value="[@members_dob/]">
                <label for="inputDoB">Date of Birth</label>
                <input type="hidden" id="inputId" value="[@id/]">
              </div>

               <div class="form-label-group">                
                <input type="text" id="inputAPIKey" class="form-control" value="[@members_activation_code/]" readonly>
                <label for="inputAPIKey">API Key</label>
                <input type="hidden" id="inputId" value="[@id/]">
              </div>

              <div class="form-label-group">
                <input type="password" id="inputNewPassword" class="form-control">
                <label for="inputNewPassword">New Password</label>
              </div>
              <div class="form-label-group">
                <input type="password" id="inputNewPasswordAgain" class="form-control">
                <label for="inputNewPasswordAgain">Type password again</label>
              </div>              

              <button id="updateButton" class="btn btn-lg btn-info btn-block text-uppercase" type="submit">Update</button>
            </form>
          </div>
        </div>

        <div id="updateSuccess"></div>
      </div>

    </div>
</main>
[@footer/]
