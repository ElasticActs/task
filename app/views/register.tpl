[@header/]
<main class="main-container container">

    <!-- Main Row -->
    <div class="row no-gutters">

      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">User Register</h5>
            <form id="registerForm" class="form-signin">

              <div class="form-label-group">                
                <input autocomplete="off" type="text" id="inputNewEmail" class="form-control" placeholder="your@Email.com" required>
                <label for="inputNewEmail">Email address</label>
                <div id="validationMessage"></div>
              </div>

              <div class="form-label-group">
                <input autocomplete="off" type="text" id="inputFirst" class="form-control" placeholder="First Name" required>
                <label for="inputFirst">First Name</label>
              </div>

              <div class="form-label-group">                
                <input autocomplete="off" type="text" id="inputLast" class="form-control" placeholder="Last Name" required>
                <label for="inputLast">Last Name</label>
              </div>

               <div class="form-label-group">                
                <input autocomplete="off" type="date" id="inputDoB" class="form-control" required>
                <label for="inputDoB">Date of Birth</label>
              </div>

              <div class="form-label-group">
                <input autocomplete="off" type="password" id="inputNewPassword" class="form-control" required>
                <label for="inputNewPassword">New Password</label>
                <div id="strengthMessage"></div>
              </div>
              <div class="form-label-group">
                <input autocomplete="off" type="password" id="inputNewPasswordAgain" class="form-control" required>
                <label for="inputNewPasswordAgain">Type password again</label>
              </div>              

              <button id="registerButton" class="btn btn-lg btn-success btn-block text-uppercase" type="submit">Register</button>
            </form>
          </div>
        </div>

        <div id="registerSuccess"></div>
      </div>

    </div>
</main>
[@footer/]
