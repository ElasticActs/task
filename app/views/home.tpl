[@header/]
<main class="main-container container">

    <!-- Main Row -->
    <div class="row no-gutters">

      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Sign In</h5>
            <form id="loginForm" class="form-signin">
              <div class="form-label-group">
                <input autocomplete="off" type="text" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                <label for="inputEmail">Email address</label>
              </div>

              <div class="form-label-group">
                <input autocomplete="off" type="password" id="inputPassword" class="form-control" required>
                <label for="inputPassword">Password</label>
              </div>

              <button id="submitButton" class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
            </form>
          </div>

        </div>
        <div id="success"></div>
      </div>

    </div>
</main>
[@footer/]
