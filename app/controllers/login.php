<?php

namespace APP\controllers;

use Flight;

class login
{
    private $host;
    private $auth;
    private $pw;
    private $user;
    private $loginInfo;
    private $token;
    private $userId;
    public $returnInfo = array();

    public function __construct() {
        $this->host  = $_SERVER['HTTP_HOST'];
        $this->user = new \App\models\memberModel();
        $this->pw = new \Libs\libraries\getHandlePasswords();
        $this->auth = new \Libs\libraries\getBearerAuth();
    }

    public function login()
    {
        $this->data = Flight::request()->data;
        if (is_null($this->data['password']) || is_null($this->data['email']) || !filter_var($this->data['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $this->loginInfo = $this->user->getLogin($this->data['email']);
        if (count($this->loginInfo) != 1) {
            return false;
        }

        if ($this->pw->validatePassword($this->data['password'], $this->loginInfo[0]['members_password'])) {
            $this->token = $this->loginInfo[0]['members_activation_code'];
            $this->userId = $this->loginInfo[0]['id'];
            $this->returnInfo = array('userToken' => $this->token, 'userId' => $this->userId);
            
            $this->user->updateCounter($this->userId);

            header('Authorization: Bearer '. $this->token);
            setcookie('authToken', $this->token, time() + (86400 * 30), "/"); // 86400 = 1 day
            setcookie('authId', $this->userId, time() + (86400 * 30), "/"); // 86400 = 1 day

            echo(json_encode($this->returnInfo));
    
            return true;
        }

        return false;
    }

    public function logout() 
    {
        setcookie("authToken", "", time() - 3600);
        setcookie("authId", "", time() - 3600);

        $response = ['code' => 200, 'message' => 'OK'];
        Flight::json($response);
        
        return;
    }
}