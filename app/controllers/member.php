<?php

namespace APP\controllers;

use Libs\libraries\getHandlePasswords;
use Libs\libraries\getEmail;
use App\models\memberModel;
use Flight;

class member
{
    private $pw;
    private $user;
    private $allUsers;
    private $singleMember;
    private $data;
    private $updateMember;
    private $email;
    private $emailBody;
    private $auth;

    public function __construct() {
        $this->auth = new \Libs\libraries\getBearerAuth();
        $this->user = new memberModel();
        $this->pw = new getHandlePasswords();
        $this->email = new getEmail();
    }

    /**
     * 
     */
    public function allMembers()
    {
        if (!$this->auth->getBearerToken()) {
            $response = ['code' => 403, 'message' => 'Fail to authenticate access token!'];
            Flight::json($response);

            return;
        }

        $this->allUsers = $this->user->getAllUsers();
        Flight::json($this->allUsers);
    }

    /**
     * 
     */
    public function singleMember(int $userId) 
    { 
        if (!$this->auth->getBearerToken()) {
            $response = ['code' => 403, 'message' => 'Fail to authenticate access token!'];
            Flight::json($response);
            
            return;
        }

        $this->singleMember = $this->user->getUser($userId);

        Flight::json($this->singleMember);
    }

    /**
     * 
     */
    public function updateMember() 
    {
        // we only want the data sent to this request so we access it's 'data' member variable
        $this->data = Flight::request()->data;
        
        if (empty($this->data['password'])) {
            unset($this->data['password']);    
        } else {
            $this->data['password'] = $this->pw->encryptPassword($this->data['password']);
        }

        if (isset($this->data['id']) && !is_null($this->data['id']) && is_numeric($this->data['id'])) {      
            // Need to authenticate to update
            if (!$this->auth->getBearerToken()) {
                $response = ['code' => 403, 'message' => 'Fail to authenticate access token!'];
                Flight::json($response);
                
                return;
            }

            $this->updateMember = $this->user->updateUser($this->data);
            $this->updateMember['code'] = 200;
            Flight::json($this->updateMember);

            return;
        } else {
            // openssl_random_pseudo_bytes - Generate a pseudo-random string of bytes
            // bin2hex — Convert binary data into hexadecimal representation
            $this->data['activationCode'] = bin2hex(openssl_random_pseudo_bytes(4));

            $this->updateMember = $this->user->addUser($this->data);
            $this->updateMember['code'] = 200;
            Flight::json($this->updateMember);
    
            // Create the email and send the message
            $this->emailBody['to'] = $this->data['email'];
            $this->emailBody['toName'] = $this->data['firstName'] .' '. $this->data['lastName'];
            $this->emailBody['subject'] = "Please confirm your email";
            $this->emailBody['body'] = "<strong>Please verify your email by clicking below link.</strong><br><a href='http://task.local/confirm/{$this->data['activationCode']}'>http://task.local/confirm/{$this->data['activationCode']}</a>";

            $this->email->sendEmail($this->emailBody);            
        }
    }    

    /**
     * 
     */
    public function deleteMember(int $userId) 
    {
        if (!$this->auth->getBearerToken()) {
            $response = ['code' => 403, 'message' => 'Fail to authenticate access token!'];
            Flight::json($response);
            
            return;
        }

        $this->singleMember = $this->user->deleteUser($userId);
        $this->singleMember['code'] = 200;
        Flight::json($this->singleMember);
    }

    public function confirmEmail(string $id)
    {
        $this->singleMember = $this->user->getUserByKey($id);
        if (count($this->singleMember) == 1) {
            $this->user->updateConfirmation($id);
            $response = ['code' => 200, 'mTitle' => "<h5 class='card-title text-center'>Email was confirmed</h5>", 'message' => "{$this->singleMember[0]['members_firstName']}, now you can login with your email and password!"];
            Flight::json($response);

        } else {
            $response = ['code' => 204, 'mTitle' => "<h5 class='card-title text-center'>Fail to confirm your email</h5>", 'message' => 'Something went wrong. Please try again later'];
            Flight::json($response);
        }

    }

    public function checkExitEmail()
    {
        $this->data = Flight::request()->data;
        $this->singleMember = $this->user->getUserByEmail($this->data['email']);
        if (count($this->singleMember) == 1) {
            $response = ['code' => 204, 'message' => 'We already have this email!'];
            Flight::json($response);
        } else {
            $response = ['code' => 200, 'message' => "OK"];
            Flight::json($response);
        }

    }
}