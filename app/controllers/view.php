<?php

namespace APP\controllers;

use Flight;

class view
{
    private $host;
    private $menuOne;
    public $basicTemplateInfo;
    public $view;
    public $client;

    public function __construct() {
        $this->host  = $_SERVER['HTTP_HOST'];
        $this->view = new \Libs\libraries\getViews();
        $this->client = new \GuzzleHttp\Client();
    }

    public function getLoginPage()
    {
        $this->menuOne = '<li class="nav-item"><a class="nav-link" href="/register">Register</a></li><li class="nav-item"><a class="nav-link" href="/">Login</a></li>';

        if (isset($_COOKIE['authToken']) && !empty($_COOKIE['authToken'])) {  
            if (isset($_COOKIE['authId']) && !empty($_COOKIE['authId'])) { 
                $this->menuOne = '<li class="nav-item"><a class="nav-link" href="/userinfo/'. $_COOKIE['authId'] .'">User Information</a></li><li class="nav-item"><a class="nav-link" id="logOut" href="#">Logout</a></li>';
            }
        }
        
        $this->basicTemplateInfo = array(
            'metaDescription' => 'task Login', 
            'canonicalLink' => '/',
            'menuOne' => $this->menuOne
        );
        $this->view->view('home', $this->basicTemplateInfo);
    }

    public function getRegisterPage() 
    {
        $this->basicTemplateInfo = array(
            'metaDescription' => 'user Information', 
            'canonicalLink' => "/register/",
            'menuOne' => '<li class="nav-item"><a class="nav-link" href="/register">Register</a></li><li class="nav-item"><a class="nav-link" href="/">Login</a></li>'
        );

        $this->view->view('register', $this->basicTemplateInfo);
    }

    public function getUserInfoPage(int $userId) 
    {
        if (isset($_COOKIE['authToken']) && !empty($_COOKIE['authToken'])) {    
            $result = $this->client->request('GET', "http://{$this->host}/user/".$userId,  ['headers' => 
                        ['Authorization' => "Bearer {$_COOKIE['authToken']}"]
            ]);

            $response = json_decode($result->getBody(), true);
            // print_r($response);
            if ($response) {
                $target = $response['members_activation_code'];

                if ($_COOKIE['authToken'] === $target) {
                    $this->basicTemplateInfo = array(
                        'metaDescription' => 'user Information', 
                        'canonicalLink' => "/userinfo/{$userId}",
                        'menuOne' => '<li class="nav-item"><a class="nav-link" href="/userinfo/'.$response['id'].'">User Information</a></li><li class="nav-item"><a class="nav-link" id="logOut" href="#">Logout</a></li>'
                    );
        
                    $this->view->view('userInfo', array_merge($this->basicTemplateInfo, $response));
    
                    return;
                }
            }
        }

        header("Location: http://{$this->host}/");
        exit();
    }

    public function getConfirmPage(string $userId)
    {
        $this->basicTemplateInfo = array(
            'metaDescription' => 'user Information', 
            'canonicalLink' => "/confirm/{$userId}",
            'menuOne' => '<li class="nav-item"><a class="nav-link" href="/register">Register</a></li><li class="nav-item"><a class="nav-link" href="/">Login</a></li>'
        );

        $result = $this->client->request('GET', "http://{$this->host}/confirm-user/".$userId);
        $response = json_decode($result->getBody(), true);

        $this->view->view('confirm', array_merge($this->basicTemplateInfo, $response));
    }
}