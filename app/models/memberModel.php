<?php

namespace App\models;

use Libs\configs\workDB;
use Flight;

class memberModel
{
    /**
     * @var
     */
    private $db;

    private $query;
    private $condition;

    private $actionElement;

    private $newInfo;
    private $lastInsertedId;

    /**
     * @var array
     */
    public $userInfo = array();

    /**
     * memberModel constructor.
     */
    public function __construct()
    {
        $this->db = new workDB();
    }

    public function getAllUsers() : array
    {
        $this->condition = array(
            'AND' => array(
                'members_publish = \'1\'',
                'members_status = \'1\''
            )
        );

        $this->actionElement = array(
            'action'    => 'select',
            'table'     => 'members',
            'data'      => array (),
            'bind'      => array(),
            'condition' => $this->condition,
            'range'     => 'all'
        );

        $this->userInfo = $this->db->getDBAction($this->actionElement);

        if (!$this->userInfo) {
            // Set a 400 (bad request) response code and exit.
            $this->userInfo = array('code' => 403, 'message' => 'There is no data exist!');
        }

        return $this->userInfo;
    }

    public function getUserByEmail(string $key) : array
    {
        $this->condition = array(
            'AND' => array(
                'members_email = :key',
                'members_publish = \'1\'',
                'members_status = \'1\''
            )
        );

        $this->actionElement = array(
            'action'    => 'select',
            'table'     => 'members',
            'data'      => array (),
            'bind'      => array(':key' => $key),
            'condition' => $this->condition,
            'range'     => 'all'
        );

        $this->userInfo = $this->db->getDBAction($this->actionElement);

        if (!$this->userInfo) {
            // Set a 400 (bad request) response code and exit.
            $this->userInfo = array('code' => 403, 'message' => 'There is no data exist!');
        }

        return $this->userInfo;
    }

    public function getUserByKey(string $key) : array
    {
        $this->condition = array(
            'AND' => array(
                'members_activation_code = :key',
                'members_publish = \'1\'',
                'members_status = \'1\''
            )
        );

        $this->actionElement = array(
            'action'    => 'select',
            'table'     => 'members',
            'data'      => array (),
            'bind'      => array(':key' => $key),
            'condition' => $this->condition,
            'range'     => 'all'
        );

        $this->userInfo = $this->db->getDBAction($this->actionElement);

        if (!$this->userInfo) {
            // Set a 400 (bad request) response code and exit.
            $this->userInfo = array('code' => 403, 'message' => 'There is no data exist!');
        }

        return $this->userInfo;
    }

    public function getUser(int $userId) : array
    {
        $this->condition = array(
            'AND' => array(
                'id = :userId',
                'members_publish = \'1\'',
                'members_status = \'1\''
            )
        );

        $this->actionElement = array(
            'action'    => 'select',
            'table'     => 'members',
            'data'      => array (),
            'bind'      => array(':userId' => $userId),
            'condition' => $this->condition,
            'range'     => 'single'
        );

        $this->userInfo = $this->db->getDBAction($this->actionElement);

        if (!$this->userInfo) {
            // Set a 400 (bad request) response code and exit.
            $this->userInfo = array('code' => 403, 'message' => 'There is no data or more than one exist!');
        }

        return $this->userInfo;
    }

    public function addUser(object $userInfo)
    {
        $this->query = "INSERT INTO members (members_firstName, members_lastName, members_dob, members_email, members_password, members_activation_code, members_email_confirmed, members_visited, members_registered_date, members_activated_date, members_modified_date, members_date_of_last_login, members_publish, members_status)
        VALUES (:firstName, :lastName, :dob, :email, :pw, :activationCode, 0, 0, now(), now(), now(), now(), 1, 1);";
        
        $this->db->getQuery($this->query);
        $this->db->getExecute(
            array(
                ':firstName' => $userInfo['firstName'],
                ':lastName' => $userInfo['lastName'],
                ':dob' => $userInfo['dob'],
                ':email' => $userInfo['email'],
                ':pw' => $userInfo['password'],
                ':activationCode' => $userInfo['activationCode']
        ));

        return array('code' => 200, 'message' => 'OK');
    }
    
    public function updateUser(object $userInfo)
    {
        try {
            if (is_null($userInfo['password'])) {
                $this->query = "UPDATE members SET members_firstName = :firstName, members_lastName = :lastName, members_dob = :dob, members_email = :email, members_modified_date = now() WHERE id = :userId;";
            
                $this->db->getQuery($this->query);
                $this->db->getExecute(array(
                    ':userId' => $userInfo['id'],
                    ':firstName' => $userInfo['firstName'],
                    ':lastName' => $userInfo['lastName'],
                    ':dob' => $userInfo['dob'],
                    ':email' => $userInfo['email']
                ));
            } else {
                $this->query = "UPDATE members SET members_firstName = :firstName, members_lastName = :lastName, members_dob = :dob, members_email = :email, members_password = :pw, members_modified_date = now() WHERE id = :userId;";
            
                $this->db->getQuery($this->query);
                $this->db->getExecute(array(
                    ':userId' => $userInfo['id'],
                    ':firstName' => $userInfo['firstName'],
                    ':lastName' => $userInfo['lastName'],
                    ':dob' => $userInfo['dob'],
                    ':email' => $userInfo['email'],
                    ':pw' => $userInfo['password']
                ));
            }

    
            return array('code' => 200, 'message' => 'OK');            
        } catch (\Exception $exception) {
            return array('code' => 400, 'message' => 'Bad Request - ' . $exception->getMessage());       
        }
    }

    public function updateCounter(int $userId)
    {
        try {
            $this->query = "UPDATE members SET members_visited = members_visited + 1, members_date_of_last_login = now(), members_modified_date = now() WHERE id = :userId;";
            
            $this->db->getQuery($this->query);
            $this->db->getExecute(array(
                ':userId' => $userId
            ));

            return array('code' => 200, 'message' => 'OK');            
        } catch (\Exception $exception) {
            return array('code' => 400, 'message' => 'Bad Request - ' . $exception->getMessage());       
        }
    }

    public function deleteUser(int $userId)
    {
        if (count($this->getUser($userId)) == 1) {
            $this->query = "DELETE FROM members WHERE id = :userId;";
            $this->db->getQuery($this->query);        
            $this->db->getExecute( array(':userId' => $userId));
    
            return array('code' => 200, 'message' => 'OK');
        } else {
            return array('code' => 403, 'message' => 'There is no data or more than one exist!');
        }
    }
    
    /**
     * 
     */
    public function getLogin(string $email) : array
    {
        $this->condition = array(
            'AND' => array(
                'members_email = :email',
                'members_email_confirmed = 1',
                'members_publish = 1',
                'members_status = 1'
            )
        );

        $this->actionElement = array(
            'action'    => 'select',
            'table'     => 'members',
            'data'      => array (),
            'bind'      => array(':email' => $email),
            'condition' => $this->condition,
            'range'     => 'all'
        );

        $this->userInfo = $this->db->getDBAction($this->actionElement);

        if (!$this->userInfo) {
            // Set a 400 (bad request) response code and exit.
            $this->userInfo = array();
        }

        return $this->userInfo;
    }

    /**
     * 
     */
    public function checkAuth(string $token) : array
    {
        $this->condition = array(
            'AND' => array(
                'members_activation_code = :token',
                'members_email_confirmed = 1',
                'members_publish = 1',
                'members_status = 1'
            )
        );

        $this->actionElement = array(
            'action'    => 'select',
            'table'     => 'members',
            'data'      => array (),
            'bind'      => array(':token' => $token),
            'condition' => $this->condition,
            'range'     => 'all'
        );

        $this->userInfo = $this->db->getDBAction($this->actionElement);

        if (!$this->userInfo) {
            // Set a 400 (bad request) response code and exit.
            $this->userInfo = array();
        }

        return $this->userInfo;
    }    

    /**
     * 
     */
    public function updateConfirmation(string $token) : array
    {
        $this->query = "UPDATE members SET members_email_confirmed = 1, members_activated_date = now() WHERE members_activation_code = :code;";
        $this->db->getQuery($this->query);
        $this->db->getExecute(array(
            ':code' => $token
        ));

        return array('code' => 200, 'message' => 'OK');
    }     
}
