<?php

namespace Libs\libraries;

use Flight;

class getBearerAuth
{
    public $access_token;
    public $member;
    public $auth;

    public function __construct() {
        $this->access_token = null;
        $this->member = new \App\models\memberModel();
    }
    
    /**
     * get access token from header
     */
    public function getBearerToken()
    {
        $access_token = Flight::request()->getVar('HTTP_AUTHORIZATION', null);
        if (empty(trim($access_token))) {
            return false;
        } else {
            $access_token = trim(str_replace('Bearer ', '', $access_token));
            $this->auth = $this->member->checkAuth($access_token);

            if (count($this->auth) == 1) {
                return true;
            }

            return false;
        }
    }
}