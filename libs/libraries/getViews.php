<?php

namespace Libs\libraries;

class getViews
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $fileName;

    private $template;

    private $found;

    private $subTemplate;

    private $optimizeHtml;

    /**
     * getViews constructor.
     */
    public function __construct()
    {
        $this->path = preg_replace('/\W\w+\s*(\W*)$/', '$1', realpath(dirname(__DIR__))) . '/app/views/';
        $this->found = 0;
    }

    /**
     * @param string $fileName
     * @param null $data
     * @return string
     */
    public function view($fileName, $data = null)
    {
        $this->setFileName($fileName);

        if ($this->checkFile($fileName)) {
            $this->template = file_get_contents($this->path . $this->fileName);
        } else {
            return 'Error! Can\'t load the template ' . $this->fileName;
        }

        $this->found = preg_match_all('/\[\@(\w+)[^\/\]]*\/\]/', $this->template, $matches);
        if ($this->found > 0) {
            $this->subTemplate = '';
            foreach ($matches[1] as $val) {
                // Check sub template
                if ($this->checkFile($val)) {
                    $this->subTemplate = file_get_contents($this->path . $val . '.tpl');

                    $this->template = str_replace('[@'.$val.'/]', $this->subTemplate, $this->template);
                }
            }

            if (!is_null($data) && is_array($data)) {
                foreach ($data as $key => $value) {
                    $this->template = str_replace('[@'.$key.'/]', $value, $this->template);
                }
            }

            $this->template = preg_replace('/\[\@(\w+)[^\/\]]*\/\]/', '', $this->template);

            echo $this->template;
        }
    }

    /**
     * @param string $name
     */
    private function setFileName($name)
    {
        $this->fileName = $name . '.tpl';
    }

    /**
     * @param $name | string
     * @return bool
     */
    private function checkFile($name)
    {
        if (file_exists($this->path . $name . '.tpl')) {
            return true;
        }

        return false;
    }
}
