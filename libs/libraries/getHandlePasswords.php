<?php

namespace Libs\libraries;

/**
 * Class handle passwords
 */
class getHandlePasswords
{
    private $pwSault;
    private $checkingMatching;
    private $makePW;
    private $encryptedPW;

    public function __construct()
    {
        $this->pwSault          = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $this->checkingMatching = '#.*^(?=.{7,12})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#';
        $this->makePW           = '';
        $this->encryptedPW      = '';
    }

    // Validates a plain text password with an encrpyted password
    public static function validatePassword( $plain, $encrypted )
    {
        if (!is_null( $plain ) && !is_null( $encrypted ) ) {
            // split apart the hash / salt
            $stack = explode( ':', $encrypted );
            if( sizeof( $stack ) != 2 ){
                return false;
            }
            
            if ( md5( $stack[1] . $plain ) == $stack[0] ){
                return true;
            }
        }

        return false;
    }

    // Checking password - 7 to 12  /  at least one CAPS / one letter / one number
    public function checkPassword( $password )
    {
        if ( !preg_match( $this->checkingMatching, $password ) ){
            return false;
        } else {
            return true;
        }
    }

    // Encrypting password
    public function encryptPassword( $plain )
    {
        for ( $i=0; $i<10; $i++ ){
            $this->encryptedPW .= $this->randNum();
        }

        $salt = substr( md5( $this->encryptedPW ), 0, 2 );
        $this->encryptedPW = md5( $salt . $plain ) . ':' . $salt;

        return $this->encryptedPW;
    }

    // Making 12 length of password
    public function makePassword( $length = 12 )
    {
        mt_srand( 10000000*( double )microtime() );
        for ( $i = 0; $i < $length; $i++ ){
            $this->makePW .= $this->pwSault[mt_rand( 0,61 )];
        }

        return $this->makePW;
    }

    // Generate random numbers
    public function randNum( $min = null, $max = null )
    {
        static $seeded;

        if ( !isset( $seeded ) ){
            mt_srand( ( double )microtime()*1000000 );
            $seeded = true;
        }

        if ( isset( $min ) && isset( $max ) ){
            if ( $min >= $max ){
                return $min;
            } else {
                return mt_rand( $min, $max );
            }
        } else {
            return mt_rand();
        }
    }
}
