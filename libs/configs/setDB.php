<?php

namespace Libs\configs;

use Flight;
use PDO;

class setDB
{
    protected $dbConnect;

    protected $dbInfo = array();
    
    protected function __construct() 
    {
        $this->dbInfo = [
            'DB_SERVER'   => 'localhost',   // This is normally set to localhost
            'DB_USERNAME' => 'homestead',   // MySQL Username
            'DB_PASSWORD' => 'secret',      // MySQL Password
            'DB_NAME'     => 'task_db'      // MySQL Database name
        ];
    }

    /**
     * open Database connection
     */
    protected function getOpenDB()
    {
        try {    
            Flight::register('db', 'PDO', array('mysql:host='. $this->dbInfo['DB_SERVER'] .';dbname='. $this->dbInfo['DB_NAME'] .'', $this->dbInfo['DB_USERNAME'], $this->dbInfo['DB_PASSWORD'],
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8 COLLATE utf8_general_ci")),
                function($db) {
                    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                }
            );

            // New instance of the class
            $this->dbConnect = Flight::db(false);
        } catch(\PDOException $e) {
            header('HTTP/1.1 503 Service Unavailable'. $e->getMessage());
        }

        return $this->dbConnect;
    }


    /**
    * close Database connection
    */
    protected function getCloseDB()
    {
        if ( isset($this->dbConnect) && ! is_null($this->dbConnect) ) {
            $this->dbConnect = null;
        }
    }
}