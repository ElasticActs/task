<?php

namespace Libs\configs;

use PDO;
/**
 * Class for Database connection.
 */
class workDB extends setDB
{
    private static $connect;
    public static $stmt;

    /**
     * workDB constructor.
     */
    public function __construct()
    {
        parent::__construct();
        workDB::$connect = $this->getOpenDB();

        /**
         * Check the tables exist or not
         */
        $query = "SHOW TABLES";
        workDB::getQuery($query);
        $result = workDB::getExecuteFetchAll(array());

        try {    
            if (!$result) {
                throw new \Exception("No db tables set yet.");
            }
        } catch (\Exception $exception) {
            header('HTTP/1.1 503 Service Unavailable - ' . $exception->getMessage());
            die($exception->getMessage());
        }
    }

    /**
     * @param $db_query
     */
    public static function getQuery($db_query)
    {
        //It takes away the threat of SQL Injection
        workDB::$stmt = workDB::$connect->prepare($db_query);
    }

    /**
     * Bind the inputs with the placeholders
     * $param placeholder like :name
     *
     * @param $param
     * @param $value
     */
    public static function getBind($param, $value)
    {
        if (is_int($value)) {
            $type = PDO::PARAM_INT;
        } elseif (is_bool($value)) {
            $type = PDO::PARAM_BOOL;
        } elseif (is_null($value)) {
            $type = PDO::PARAM_NULL;
        } else {
            $type = PDO::PARAM_STR;
        }

        // workDB::$stmt->bindParam(':'.$param, $value);
        workDB::$stmt->bindValue(':'.$param, $value, $type);
    }

    public static function getDBAction($action)
    {
        reset($action['data']);
        switch ($action['action']) {
            case 'insert':
                $query = 'INSERT INTO ' . $action['table'] . ' ( ';

                foreach ($action['data'] as $columns => $v) {
                    $query .= $columns . ', ';
                }

                $query = substr( $query, 0, -2 ) . ' ) VALUES ( ';
                reset($action['data']);

                foreach ($action['data'] as $c => $value) {
                    switch ((string)$value) {
                        case 'now()':
                            $query .= 'now(), ';
                            break;
                        case '':
                        case 'null':
                            $query .= 'null, ';
                            break;
                        default:
                            $query .= '\''. $value .'\', ';
                            break;
                    }
                }
                $query = substr( $query, 0, -2 ) . ' )';
                break;
            case 'update':
                $query = 'UPDATE ' . $action['table'] . ' SET ';

                foreach ($action['data'] as $columns => $value) {
                    switch ((string)$value) {
                        case 'now()':
                            $query .= $columns . ' = now(), ';
                            break;
                        case '':
                        case 'null':
                            $query .= $columns .= ' = null, ';
                            break;
                        default:
                            $query .= $columns . ' = \'' . $value . '\', ';
                            break;
                    }
                }

                $query = substr( $query, 0, -2 ) . ' WHERE ' . workDB::conditionMaker($action['condition']);
                break;
            default:
            case 'select':
                if (count($action['data']) == 0) {
                    $query = 'SELECT * ';
                } else {
                    $query = 'SELECT ';
                    foreach ($action['data'] as $key => $value) {
                        $query .= $value . ', ';
                    }
                }

                $query = substr($query, 0, -1) . ' FROM ' . $action['table'];

                if(!empty( $action['condition'])){
                    $query = substr($query, 0) . ' WHERE ' . workDB::conditionMaker($action['condition']);
                }
                break;
        }

        workDB::getQuery($query);

        switch ($action['range']) {
            case 'single':
                $result = workDB::getExecuteFetch($action['bind']);
                break;
            case 'all':
                $result = workDB::getExecuteFetchAll($action['bind']);
                break;
            default:
                $result = workDB::getExecute($action['bind']);
                break;
        }

        return $result;
    }

    /**
     *
     */
    public static function getExecute($bindValue)
    {
        try {
            workDB::$stmt->execute($bindValue);
        } catch (\PDOException $exception) {
            echo 'workDB: '. $exception->getMessage();
        }
    }

    /**
     * returns an array of the result set rows
     *
     * @param null $value
     * @return mixed
     */
    public static function getExecuteFetchAll($bindValue = null, $value = null)
    {
        workDB::getExecute($bindValue);

        $type = $value === 'OBJ' ? PDO::FETCH_OBJ : PDO::FETCH_ASSOC;
        $result = workDB::$stmt->fetchAll($type);

        workDB::$stmt->closeCursor();

        return $result;
    }

    /**
     * returns a single record from the database
     *
     * @param null $value
     * @return mixed
     */
    public static function getExecuteFetch($bindValue = null, $value = null)
    {
        workDB::getExecute($bindValue);

        $type = $value === 'OBJ' ? PDO::FETCH_OBJ : PDO::FETCH_ASSOC;
        $result = workDB::$stmt->fetch($type);

        workDB::$stmt->closeCursor();

        return $result;
    }

    /**
     *  Returns number  of columns
     *
     *  @param  string $query
     *  @param  array  $params
     *  @return string
     */
    public static function getSingle($bindValue = null)
    {
        workDB::getExecute($bindValue);
        $result = workDB::$stmt->fetchColumn();

        // Frees up the connection to the server so that other SQL statements may be issued
        workDB::$stmt->closeCursor();

        return $result;
    }

    /**
     * returns the number of effected rows from the previous delete, update or insert statement
     *
     * @return mixed
     */
    public static function getRowCount()
    {
        return workDB::$stmt->rowCount();
    }

    /**
     * returns the last inserted Id as a string
     *
     * @return mixed
     */
    public static function getLastInsertId()
    {
        return workDB::$stmt->lastInsertId();
    }

    /**
     * Truncate table
     *
     * @param $table
     */
    public static function truncateTB($table)
    {
        exec("TRUNCATE TABLE $table");
    }

    /**
     *
     * @param $data array
     */
    public static function conditionMaker($data)
    {
        if (is_array($data)) {
            $idx = 0;
            $condition = '';
            foreach ($data as $idxA => $valueA) {
                foreach ($valueA as $idxB => $valueB) {
                    if ($idx > 0) {
                        $condition .= $idxA . ' ';
                    }

                    if (!is_numeric($idxB)) {
                        $subIdx = 0;
                        foreach($valueB as $idxC => $valueC) {
                            if ($subIdx == 0) {
                                $condition .= ' (';
                            }

                            if ($subIdx > 0) {
                                $condition .= $idxB . ' ';
                            }

                            $condition .= $valueC . ' ';

                            if (count($valueB) -1 == $subIdx) {
                                $condition .= ') ';
                            }

                            $subIdx ++;
                        }

                    } else {
                        $condition .= $valueB . ' ';
                    }

                    $idx++;
                }
            }
        } else {
            $condition = $data;
        }


        return $condition;
    }
}
