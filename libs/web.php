<?php

namespace Libs;

use Flight;

class web
{
    public $login;
    public $view;
    public $member;

    public function __construct()
    {
        $this->login = new \App\controllers\login();
        $this->view = new \App\controllers\view();
        $this->member = new \App\controllers\member();        
    }

    public function getRoute()
    {
        Flight::route('/', function() {
            $this->view->getLoginPage();
        });
        
        Flight::route('/register', function() {
            $this->view->getRegisterPage();
        });
        
        // get single user by id
        Flight::route('/userinfo/@id', function($id) {
            $this->view->getUserInfoPage($id);
        });
        
        // Confirm single user by id
        Flight::route('/confirm/@id', function($id) {
            $this->view->getConfirmPage($id);
        });
        
        Flight::route('POST /login', function() {
            $this->login->login();
        });
        
        Flight::route('GET /logout', function() {
            $this->login->logout();
        });

        Flight::route('GET /confirm-user/@id', function($id) {
            $this->member->confirmEmail($id);
        });
        
        // Get all users
        Flight::route('GET /all-users', function() {
            $this->member->allMembers();
        });
        
        // Get single user information by id
        Flight::route('GET /user/@id', function($id) {
            $this->member->singleMember($id);
        });
        
        // Add/Update single user information
        Flight::route('POST /user', function() {
            $this->member->updateMember();
        });
        
        Flight::route('POST /check-email', function() {
            $this->member->checkExitEmail();
        });

        // Remove single user by id
        Flight::route('DELETE /user/@id', function($id) {
            $this->member->deleteMember($id);
        });
        
        // Handle not found
        Flight::map('notFound', function() {
            // Display custom 404 page
            // include 'errors/404.html';
            Flight::halt(404, 'Display custom 404 page');
            Flight::stop();
        });
    }
}
