# Task
1. Register:
    1.1 Name
    1.2 Email
    1.3 Date of birth
    1.4 Password
2. Confirm email
3. Login
4. See information and update it

### Composer 
- You need to run composer to install all of packages
```sh
composer install
```

### Database 
```sh
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `members_firstName` varchar(32) NOT NULL DEFAULT '',
  `members_lastName` varchar(32) NOT NULL DEFAULT '',
  `members_dob` date NOT NULL DEFAULT '0000-00-00',
  `members_email` varchar(96) NOT NULL DEFAULT '',
  `members_password` varchar(40) NOT NULL DEFAULT '',
  `members_activation_code` mediumtext NOT NULL,
  `members_email_confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `members_visited` int(11) NOT NULL DEFAULT '0',
  `members_registered_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `members_activated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `members_modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `members_date_of_last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `members_publish` tinyint(1) NOT NULL DEFAULT '1',
  `members_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `members` (`id`, `members_firstName`, `members_lastName`, `members_dob`, `members_email`, `members_password`, `members_activation_code`, `members_email_confirmed`, `members_visited`, `members_registered_date`, `members_activated_date`, `members_modified_date`, `members_date_of_last_login`, `members_publish`, `members_status`) VALUES
(1, 'Ken', 'Doe', '2020-08-01', 'iweb@kenwoo.ca', '682ca46c758d640effee6014741ddf48:fb', '22b75693', 1, 0, '2020-08-01 00:00:00', '2020-08-01 00:00:00', '2020-08-01 00:00:00', '2020-08-01 00:00:00', 1, 1);

INSERT INTO `members` (`id`, `members_firstName`, `members_lastName`, `members_dob`, `members_email`, `members_password`, `members_activation_code`, `members_email_confirmed`, `members_visited`, `members_registered_date`, `members_activated_date`, `members_modified_date`, `members_date_of_last_login`, `members_publish`, `members_status`) VALUES
(2, 'Woo', 'K', '2000-07-17', 'dev@kenwoo.ca', '2d83ede23195af3c56639eeeea51c4b2:4c', '185ad046', 0, 0, '2020-08-01 00:00:00', '2020-08-01 00:00:00', '2020-08-01 00:00:00', '2020-08-01 00:00:00', 1, 1);

ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `members_email` (`members_email`);

ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;
```

### API
| Mathod | URL |
| ------ | ------ |
| POST | /login |
| GET | /logout |
| GET | /confirm-user/@id |
| GET | /all-users |
| GET | /user/@id |
| POST | /user |
| DELETE | /user/@id |
  - You need Authorization Bearer TOKEN in the header
  - You can use '22b75693' as a TOKEN

### How to use 
     After your registeration, you need to confirm your email by cliking the linck which emaied you at the registration.
     It will look like below.
     > http://task.local/confirm/22b75693
     Other than that, everything is very straightforward.
     
     Test accounts: 
     cofirmaed account:  iweb@kenwoo.ca (12345) or uncofirmaed account: dev@kenwoo.ca (12345)
     cofirmaed API Key:  22b75693 or uncofirmaed API Key: 185ad046
 
### Todos

 - Write more validations and Tests
 - Write more error handling 

