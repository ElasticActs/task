<?php
define('_VALID_TAGS', 1);

$path = preg_replace('/\W\w+\s*(\W*)$/', '$1', realpath(dirname(__FILE__))) . '/task';
define("CONFIG_DOC_ROOT", $path);

require_once $path . '/vendor/autoload.php';


$web = new \Libs\web();
// Routing set
$web->getRoute();

Flight::start();